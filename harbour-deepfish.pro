# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = harbour-deepfish

CONFIG += sailfishapp_qml

OTHER_FILES += \
    harbour-deepfish.desktop \
    qml/cover/CoverPage.qml \
    qml/harbour-deepfish.qml \
    qml/pages/FirstPage.qml \
    qml/cover/coveractions.py \
    qml/pages/datadownloader.py \
    rpm/harbour-deepfish.changes.in \
    rpm/harbour-deepfish.spec \
    rpm/harbour-deepfish.yaml \
    translations/*.ts \
    harbour-deepfish.png

TRANSLATIONS += translations/harbour-deepfish.ts

DISTFILES += \
    qml/pages/Settings.qml \
    qml/pages/SettingsPage.qml
