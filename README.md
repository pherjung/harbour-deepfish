# Deepfish, DeepL for Sailfish OS

Requires an API Token to work, you can get one for free at deepl.com by signing
up. Insert the API token in the settings page.

# Current priorities I'd like to solve :

 1. Follow best practices and [coding conventions](https://sailfishos.org/wiki/Coding_Conventions) to make the code more maintainable for outsiders.
    - Getting rid of useless code
    - Better responsiveness
    - Solve UI bugs (button following long "toTranslate" text)
 2. Solve the issue with internationalisation (example, I've done a German
translation [translations/harbour-deepfish-de.ts]), but it doesn't seem to be
working. I must have forgotten something.
 3. A better cover page based on the translated text size (small translation
can be displayed on the cover, and something else for bigger, longer,
translations).
 4. Follow Jolla's [UI Definition of
Done](https://sailfishos.org/wiki/UI_Definition_of_Done) and good [Saifilsh app
development patterns](https://sailfishos.org/develop/docs/silica/sailfish-application-pitfalls.html/).
