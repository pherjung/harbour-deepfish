/*
  Copyright (C) 2015 Jolla Ltd.
  Contact: Thomas Perl <thomas.perl@jollamobile.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4

CoverBackground {
    id: containing_rect
    TextArea {
        id: text_area
        anchors.top: parent.top
        anchors.left: parent.left
        // anchors.centerIn: parent
        height: parent.height
        width: parent.width
        text: settings.original + ' = ' + settings.translation
        wrapMode: Text.WordWrap

        // text: settings.original + ' = ' + settings.translation
        // wrapMode: TextInput.Wrap
        description: settings.original_lang + qsTr(' To ') + settings.translation_lang
    }
//    states: [
//            State {
//                name: "wide text"
//                when: containing_rect.text.length > 5
//                PropertyChanges {
//                    target: containing_rect
//                    width: 200
//                    height: text_area.paintedHeight
//                }
//            },
//            State {
//                name: "not wide text"
//                when: containing_rect.text.length <= 5
//                PropertyChanges {
//                    target: containing_rect
//                    width: dummy_text.paintedWidth
//                    height: text_area.paintedHeight
//                }
//            }
//        ]
}
