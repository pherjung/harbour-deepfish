import QtQuick 2.6
import Nemo.Configuration 1.0

ConfigurationGroup {
    id: settingsToken

    path: "/apps/deepl"

    property string token
    property string original
    property string translation
    property string original_lang
    property string translation_lang
}
