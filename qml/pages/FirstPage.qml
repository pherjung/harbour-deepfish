/*
  Copyright (C) 2013 Jolla Ltd.
  Contact: Thomas Perl <thomas.perl@jollamobile.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*
  Due to BSD licence I'm leaving Jolla's notice on the code.
  This file is the main page to interact with Deepfish's app.
  The code is heavily/totaly inspired by Jolla's pythonsample
  app from their website. Therefore I left Jolla's comments
  from their previous code untouched.
*/

import QtQuick 2.6
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.5

Page {

    id: firstPage
    property bool downloading : false
    property string source : "None"
    property string destination : "BG"

    SilicaFlickable {
        anchors.fill: parent
        contentHeight: column.height + Theme.paddingLarge

        contentWidth: parent.width

        PullDownMenu {
            id: pullDownMenu

            MenuItem {
                text: qsTr("Settings")
                onClicked:  pageStack.push(Qt.resolvedUrl("SettingsPage.qml"))
            }
        }

        Column {
            id: column
            spacing: Theme.paddingLarge
            width: parent.width

            PageHeader { title: qsTr("Translation") }

            TextArea {
                id: toTranslate
                height: Screen.height / 3
                text: ""
                label: qsTr("Original")
                EnterKey.enabled: text.length > 0
                EnterKey.iconSource: "image://theme/icon-m-enter-next"
                EnterKey.onClicked: {
                    focus = false
                    python.startDownload(toTranslate.text);
                    settings.original = toTranslate.text;
                    settings.original_lang =  source;
                    settings.translation_lang = destination;
                }

                rightItem: IconButton {
                    onClicked: {
                        toTranslate.text = ""
                        translated_text.text = ""
                        focus = toTranslate
                    }
                    width: icon.width
                    height: icon.height
                    icon.source: "image://theme/icon-splus-clear"
                    opacity: toTranslate.text.length > 0 ? 1.0 : 0.0
                    Behavior on opacity { FadeAnimation {} }
                }
            }
// The following Row{} represents the language choice and the switch languages button.
            Row {
                id: setting_origin
                width: parent.width
                ComboBox {
                    width: parent.width / 2
                    label: qsTr("From")
                    id: from
                    menu: ContextMenu {
                        MenuItem {
                            text: "Auto detect"
                            id: auto_detect
                            onClicked: {
                                source = "None"
                            }
                        }
                        MenuItem {
                            text: "Bulgarian"
                            id: bg
                            onClicked: {
                                source = "BG"
                            }
                        }
                        MenuItem {
                            text: "Czech"
                            onClicked: {
                                source = "CS"
                            }
                        }
                        MenuItem {
                            text: "Danish"
                            onClicked: {
                                source = "DA"
                            }
                        }
                        MenuItem {
                            text: "German"
                            onClicked: {
                                source = "DE"
                            }
                        }
                        MenuItem {
                            text: "Greek"
                            onClicked: {
                                source = "EL"
                            }
                        }
                        MenuItem {
                            text: "English"
                            onClicked: {
                                source = "EN"
                            }
                        }
                        MenuItem {
                            text: "Spanish"
                            onClicked: {
                                source = "ES"
                            }
                        }
                        MenuItem {
                            text: "Estonian"
                            onClicked: {
                                source = "ET"
                            }
                        }
                        MenuItem {
                            text: "Finnish"
                            onClicked: {
                                source = "FI"
                            }
                        }
                        MenuItem {
                            text: "French"
                            onClicked: {
                                source = "FR"
                            }
                        }
                        MenuItem {
                            text: "Hungarian"
                            onClicked: {
                                source = "HU"
                            }
                        }
                        MenuItem {
                            text: "Italian"
                            onClicked: {
                                source = "IT"
                            }
                        }
                        MenuItem {
                            text: "Japanese"
                            onClicked: {
                                source = "JA"
                            }
                        }
                        MenuItem {
                            text: "Lithuanian"
                            onClicked: {
                                source = "LT"
                            }
                        }
                        MenuItem {
                            text: "Latvian"
                            onClicked: {
                                source = "LV"
                            }
                        }
                        MenuItem {
                            text: "Dutch"
                            onClicked: {
                                source = "NL"
                            }
                        }
                        MenuItem {
                            text: "Polish"
                            onClicked: {
                                source = "PL"
                            }
                        }
                        MenuItem {
                            text: "Portuguese"
                            onClicked: {
                                source = "PT"
                            }
                        }
                        MenuItem {
                            text: "Romanian"
                            onClicked: {
                                source = "RO"
                            }
                        }
                        MenuItem {
                            text: "Russian"
                            onClicked: {
                                source = "RU"
                            }
                        }
                        MenuItem {
                            text: "Slovak"
                            onClicked: {
                                source = "SK"
                            }
                        }
                        MenuItem {
                            text: "Slovenian"
                            onClicked: {
                                source = "SL"
                            }
                        }
                        MenuItem {
                            text: "Swedish"
                            onClicked: {
                                source = "SV"
                            }
                        }
                        MenuItem {
                            text: "Chinese"
                            onClicked: {
                                source = "ZH"
                            }
                        }
                    }
                }
                IconButton {
                    id: swapLang
                    width: icon.width
                    icon.source: "image://theme/icon-s-retweet"
                    onClicked: {
                        if (source != "None" || from.currentIndex != 0){
                            var tmp = source
                            source = destination
                            destination = tmp

                            var ids = from.currentIndex-1 //because of auto detect
                            if (to.currentIndex > 5){ // because of english american
                                if (to.currentIndex > 18){ // because of portuguese brazil
                                    from.currentIndex = to.currentIndex-1
                                }else{
                                    from.currentIndex = to.currentIndex
                                }
                            }
                            else{
                                from.currentIndex = to.currentIndex+1
                            }
                            if (ids > 5){
                                if (ids > 18){
                                    to.currentIndex = ids+2
                                }else{
                                    to.currentIndex = ids+1
                                }
                            }
                            else{
                                to.currentIndex = ids
                            }


                        }
                        else {
                            Notices.show(
                                        qsTr("Source language must be other then Auto detect"),
                                        Notice.Long,
                                        Notice.Center
                                        );
                        }
                    }
                }
                ComboBox {
                    width: parent.width / 2
                    label: qsTr("To")
                    id: to
                    menu: ContextMenu {
                        MenuItem {
                            text: "Bulgarian"
                            onClicked: {
                                destination = "BG"
                            }
                        }
                        MenuItem {
                            text: "Czech"
                            onClicked: {
                                destination = "CS"
                            }
                        }
                        MenuItem {
                            text: "Danish"
                            onClicked: {
                                destination = "DA"
                            }
                        }
                        MenuItem {
                            text: "German"
                            onClicked: {
                                destination = "DE"
                            }
                        }
                        MenuItem {
                            text: "Greek"
                            onClicked: {
                                destination = "EL"
                            }
                        }
                        MenuItem {
                            text: "English (GB)"
                            onClicked: {
                                destination = "EN-GB"
                            }
                        }
                        MenuItem {
                            text: "English (US)"
                            onClicked: {
                                destination = "EN-US"
                            }
                        }
                        MenuItem {
                            text: "Spanish"
                            onClicked: {
                                destination = "ES"
                            }
                        }
                        MenuItem {
                            text: "Estonian"
                            onClicked: {
                                destination = "ET"
                            }
                        }
                        MenuItem {
                            text: "Finnish"
                            onClicked: {
                                destination = "FI"
                            }
                        }
                        MenuItem {
                            text: "French"
                            onClicked: {
                                destination = "FR"
                            }
                        }
                        MenuItem {
                            text: "Hungarian"
                            onClicked: {
                                destination = "HU"
                            }
                        }
                        MenuItem {
                            text: "Italian"
                            onClicked: {
                                destination = "IT"
                            }
                        }
                        MenuItem {
                            text: "Japanese"
                            onClicked: {
                                destination = "JA"
                            }
                        }
                        MenuItem {
                            text: "Lithuanian"
                            onClicked: {
                                destination = "LT"
                            }
                        }
                        MenuItem {
                            text: "Latvian"
                            onClicked: {
                                destination = "LV"
                            }
                        }
                        MenuItem {
                            text: "Dutch"
                            onClicked: {
                                destination = "NL"
                            }
                        }
                        MenuItem {
                            text: "Polish"
                            onClicked: {
                                destination = "PL"
                            }
                        }
                        MenuItem {
                            text: "Portuguese (excluding Brazilian)"
                            onClicked: {
                                destination = "PT-PT"
                            }
                        }
                        MenuItem {
                            text: "Portuguese (BR)"
                            onClicked: {
                                destination = "PT-BR"
                            }
                        }
                        MenuItem {
                            text: "Romanian"
                            onClicked: {
                                destination = "RO"
                            }
                        }
                        MenuItem {
                            text: "Russian"
                            onClicked: {
                                destination = "RU"
                            }
                        }
                        MenuItem {
                            text: "Slovak"
                            onClicked: {
                                destination = "SK"
                            }
                        }
                        MenuItem {
                            text: "Slovenian"
                            onClicked: {
                                destination = "SL"
                            }
                        }
                        MenuItem {
                            text: "Swedish"
                            onClicked: {
                                destination = "SV"
                            }
                        }
                        MenuItem {
                            text: "Chinese"
                            onClicked: {
                                destination = "ZH"
                            }
                        }
                    }
                }
            }

            TextArea {
                id: translated_text
                height: Screen.height / 4
                text: ""
                label: qsTr("Translated")

                rightItem:  IconButton {
                    width: icon.width
                    height: icon.height
                    icon.source: "image://theme/icon-s-clipboard"
                    opacity: translated_text.text.length > 0 ? 1.0 : 0.0
                    onClicked: {
                        Clipboard.text = translated_text.text;
                        Notices.show(qsTr("Copied"), Notice.Short, Notice.Center);
                    }
                }
            }

            ProgressBar {
                id: dlprogress
                indeterminate: true
                label: qsTr("Translating...")
                width: parent.width
                visible: firstPage.downloading
            }
            Button {
                id: translateButton
                text: qsTr("Translate")
                enabled: {
                    !firstPage.downloading;
                    toTranslate.text.length > 0;
                }
                width: parent.width
                onClicked: {
                    python.startDownload(toTranslate.text);
                    settings.original = toTranslate.text;
                    settings.original_lang =  source;
                    settings.translation_lang = destination;
                }
            }
            Python {
                id: python

                Component.onCompleted: {
                    addImportPath(Qt.resolvedUrl('.'));
                    setHandler('finished', function(translatedText, detectedLang) {
                        firstPage.downloading = false;
                        if (from.currentIndex == 0){
                            if (detectedLang == "BG"){
                                from.currentIndex = 1
                                settings.original_lang =  "BG"
                            }
                            if (detectedLang == "CS"){
                                from.currentIndex = 2
                                settings.original_lang = "CS"
                            }
                            if (detectedLang == "DA"){
                                from.currentIndex = 3
                                settings.original_lang = "DA"
                            }
                            if (detectedLang == "DE"){
                                from.currentIndex = 4
                                settings.original_lang = "DE"
                            }
                            if (detectedLang == "EL"){
                                from.currentIndex = 5
                                settings.original_lang = "EL"
                            }
                            if (detectedLang == "EN"){
                                from.currentIndex = 6
                                settings.original_lang = "EN"
                            }
                            if (detectedLang == "ES"){
                                from.currentIndex = 7
                                settings.original_lang = "ES"
                            }
                            if (detectedLang == "ET"){
                                from.currentIndex = 8
                                settings.original_lang = "ET"
                            }
                            if (detectedLang == "FI"){
                                from.currentIndex = 9
                                settings.original_lang = "FI"
                            }
                            if (detectedLang == "FR"){
                                from.currentIndex = 10
                                settings.original_lang = "FR"
                            }
                            if (detectedLang == "HU"){
                                from.currentIndex = 11
                                settings.original_lang = "HU"
                            }
                            if (detectedLang == "IT"){
                                from.currentIndex = 12
                                settings.original_lang = "IT"
                            }
                            if (detectedLang == "JA"){
                                from.currentIndex = 13
                                settings.original_lang = "JA"
                            }
                            if (detectedLang == "LT"){
                                from.currentIndex = 14
                                settings.original_lang = "LT"
                            }
                            if (detectedLang == "LV"){
                                from.currentIndex = 15
                                settings.original_lang = "LV"
                            }
                            if (detectedLang == "NL"){
                                from.currentIndex = 16
                                settings.original_lang = "NL"
                            }
                            if (detectedLang == "PL"){
                                from.currentIndex = 17
                                settings.original_lang = "PL"
                            }
                            if (detectedLang == "PT"){
                                from.currentIndex = 18
                                settings.original_lang = "PT"
                            }
                            if (detectedLang == "RO"){
                                from.currentIndex = 19
                                settings.original_lang = "RO"
                            }
                            if (detectedLang == "RU"){
                                from.currentIndex = 20
                                settings.original_lang ="RU"
                            }
                            if (detectedLang == "SK"){
                                from.currentIndex = 21
                                settings.original_lang = "SK"
                            }
                            if (detectedLang == "SL"){
                                from.currentIndex = 22
                                settings.original_lang = "SL"
                            }
                            if (detectedLang == "SV"){
                                from.currentIndex = 23
                                settings.original_lang = "SV"
                            }
                            if (detectedLang == "ZH"){
                                from.currentIndex = 24
                                settings.original_lang = "ZH"
                            }
                        }
                        translated_text.text = translatedText;
                        settings.translation = translatedText;
                    });

                    importModule('datadownloader', function () {});

                }
                function startDownload() {
                        firstPage.downloading = true;
                        call('datadownloader.downloader.download', [toTranslate.text,
                                                                    source,
                                                                    destination,
                                                                    settings.token],
                             function() {})
                    console.log('source: ' + source)
                }

                onError: {
                    // when an exception is raised, this error handler will be called
                    console.log('python error: ' + traceback);
                }

                onReceived: {
                    // asychronous messages from Python arrive here
                    // in Python, this can be accomplished via pyotherside.send()
                    console.log('got message from python: ' + data);
                }
            }
        }
    }
}
