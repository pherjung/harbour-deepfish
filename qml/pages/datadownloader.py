#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This file is used to interact between the UI
# and the deepl library.
# It's heavily/totally inspired by the
# "datadownloader.py" file from Jolla in the
# pythonsample app example given on their website.
# I left their commentaries in the code.

import pyotherside
import threading
import deepl
# import logging - uncomment this line and also
# the two last lines of this file for logging


def slow_function(
            translation,
            lang_source,
            lang_dest,
            token
            ):
    token = "your token here"
    while True:
        translator = deepl.Translator(
            token
            )
        break
    if lang_source == "Auto" or lang_source == "None":
        result = translator.translate_text(
             translation,
             target_lang=lang_dest
        )
        pyotherside.send(
            'finished',
            result.text,
            result.detected_source_lang
            )
    else:
        result = translator.translate_text(
             translation,
             source_lang=lang_source,
             target_lang=lang_dest
        )
        pyotherside.send(
            'finished',
            result.text
            )


class Downloader:
    def __init__(self):
        # Set bgthread to a finished thread so we never
        # have to check if it is None.
        self.bgthread = threading.Thread()
        self.bgthread.start()

    def download(
            self,
            text,
            lang_source,
            lang_dest,
            token
            ):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(
            target=slow_function(
                text,
                lang_source,
                lang_dest,
                token
            )
        )
        self.bgthread.start()


downloader = Downloader()
# logging.basicConfig()
# logging.getLogger('deepl').setLevel(logging.DEBUG)
