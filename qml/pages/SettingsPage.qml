import QtQuick 2.6
import Sailfish.Silica 1.0

Dialog {
    id: settingsPage

    SilicaFlickable {
        anchors.fill: parent
        contentHeight: column.height

        Column {
            id: column
            width: parent.width
            bottomPadding: Theme.paddingLarge

            DialogHeader {
                acceptText: qsTr("Save")
                title : qsTr("API token field")
            }

            TextField {
                id: apiTextField
                label: qsTr("API token field")
                placeholderText: qsTr("Paste API token value")
                text: settings.token
                leftItem: Icon {
                    source: "image://theme/icon-m-keys"
                }

                EnterKey.enabled: text.length > 0
            }
                LinkedLabel {
                x: Theme.horizontalPageMargin
                width: parent.width - 2*x
                plainText: qsTr("You can get a free API token key at https://www.deepl.com/pro#developer .")
                }

        }
    }
    onDone: {
            if (result == DialogResult.Accepted) {
                settings.token = apiTextField.text
            }
    }

}
