<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en" sourcelanguage="en">
<context>
    <name>>FirstPage</name>
    <message>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <source>Translation</source>
        <translation>Translation</translation>
    </message>
    <message>
        <source>Original</source>
        <translation>Original</translation>
    </message>
    <message>
        <source>From</source>
        <translation>From</source>
    </message>
    <message>
        <source>To</source>
        <translation>To</translation>
    </message>
    <message>
        <source>Translated</source>
        <translation>Translated</translation>
    </message>
    <message>
        <source>Translating...</source>
        <translation>Translating...</translation>
    </message>
    <message>
        <source>Translate</source>
        <translation>Translate</translation>
</context>
</TS>
