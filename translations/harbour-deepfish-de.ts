<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE" sourcelanguage="en">
<context>
    <name>>FirstPage</name>
    <message>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <source>Translation</source>
        <translation>Übersetzung</translation>
    </message>
    <message>
        <source>Original</source>
        <translation>Original</translation>
    </message>
    <message>
        <source>From</source>
        <translation>Von</source>
    </message>
    <message>
        <source>To</source>
        <translation>Zu</translation>
    </message>
    <message>
        <source>Translated</source>
        <translation>Übersetzt</translation>
    </message>
    <message>
        <source>Translating...</source>
        <translation>Übersetzen...</translation>
    </message>
    <message>
        <source>Translate</source>
        <translation>Übersetze</translation>
</context>
</TS>
